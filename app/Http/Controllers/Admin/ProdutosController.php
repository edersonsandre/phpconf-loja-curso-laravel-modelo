<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutosRequest;
use Illuminate\Support\Facades\DB;

class ProdutosController extends Controller
{
    public $model = "Produtos";

    public function __construct()
    {
        $this->_title = "Produtos";

        parent::__construct();
    }

    public function save(ProdutosRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $data = $this->_model->_save($request->all());
            });

            $response['status'] = true;
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);
    }
}
